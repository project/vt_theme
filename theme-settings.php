<?php

/**
 * @file
 * Implements().
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * @file
 * harmony_haven theme file.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function vt_theme_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  if ($form['#attributes']['class'][0] == 'system-theme-settings') {
    $form['#attached']['library'][] = 'vt_theme/theme.setting';

    $form['vt_theme_info'] = [
      '#markup' => '<h2><br/>Advanced Theme Settings</h2><div class="messages messages--warning">Clear cache after making any changes in theme settings. <a href="../../config/development/performance">Click here to clear cache</a></div>',
    ];
  }

  // Slide show
 
$form['busi_settings']['slideshow'] = array(
    '#type' => 'details',
    '#title' => t('Front Page Banner'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['busi_settings']['slideshow']['slideshow_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Banner'),
    '#default_value' => theme_get_setting('slideshow_display', 'vt_theme'),
    '#description'   => t("Check this option to show Banner in front page. Uncheck to hide."),
  );
  // $form['busi_settings']['slideshow']['slide'] = array(
  //   '#markup' => t('You can change the title, url and image of each slide in the following Slide Setting fieldsets.'),
  // );
  // $form['busi_settings']['slideshow']['slide_num'] = [
  //   '#type' => 'number',
  //   '#title' => t('Select Number of Slider Display'),
  //   '#min' => 1,
  //   '#required' => TRUE,
  //   '#default_value' => theme_get_setting('slide_num'),
  //   '#description' => t("Enter Number of slider you want to display"),
  // ];
  //$limit = theme_get_setting('slide_num');

  //for ($i = 1; $i <= $limit; $i++) {
    // $form['busi_settings']['slideshow']['slide'] = array(
    //   '#type' => 'details',
    //   '#title' => t('Slide'),
    //   '#collapsible' => TRUE,
    //   '#collapsed' => TRUE,
    // );
    $form['busi_settings']['slideshow']['slide_title_'] = array(
      '#type' => 'textfield',
      '#title' => t('Banner Title'),
      '#default_value' => theme_get_setting("slide_title", "vt_theme"),
    );
    $form['busi_settings']['slideshow']['slide_image'] = array(
      '#type' => 'managed_file',
      '#title' => t('Banner Image'),
      '#description' => t('Use same size for all the slideshow images(Recommented size : 1920 X 603).'),
      '#default_value' => theme_get_setting("slide_image", "vt_theme"),
      '#upload_location' => 'public://',
    );
    $form['busi_settings']['slideshow']['slide_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Banner URL'),
      '#default_value' => theme_get_setting("slide_url", "vt_theme"),
    );
      $form['busi_settings']['slideshow']['slide_url_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Banner title'),
      '#default_value' => theme_get_setting("slide_url_title", "vt_theme"),
    );
  //}



}

